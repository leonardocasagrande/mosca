<?php get_header(); ?>
<?php if (have_posts()) the_post();?>
<?php 
$src = '';
if (has_post_thumbnail(get_the_ID())){
	$img_src = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full');
	$src = $img_src[0];
}
?> 
<div class="banner bg-general" style="background-image: url('<?php echo $src; ?>');background-size:cover;background-position:center center;">
	<div class="filtro">
		<div class="p-150">
			<div id="detail"></div>
				<h1 class="text-white"><?php echo get_the_title(); ?></h1>
				<div class="circle">
					<i class="text-white fas fa-arrow-down"></i>
				</div>
			</div>
		</div>
		<a href="#" class="circle d-md-none c-detail">
			<i class="fas fa-arrow-up"></i>
		</a>
	</div>
	<section class="col-lg-10 px-0 m-auto">
		<div class="col-lg-9 margin-center">
			<div class="py-3 pb-md-5 pt-md-4">
				<?php echo the_content(); ?>
			</div>
		</div>
	</section>
</div>
<?php get_footer() ?>