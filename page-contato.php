<?php get_header(); ?> <div class="banner bg-contato"><div class="filtro"><div class="p-150"><div id="detail"></div><h1 class="text-white">Contato</h1><div class="circle"><i class="text-white fas fa-arrow-down"></i></div></div></div><!-- <a href="#" class="circle d-md-none c-detail">
        <i class="fas fa-arrow-up"></i>
    </a> --></div><section class="contato pt-3 bg-gray pt-lg-5"><span class="titulo">PREENCHA O FORMULÁRIO</span><p class="sub col-lg-5 m-auto pb-lg-5 pb-3 d-block">Em que a Mosca Logística pode te ajudar? Deixe aqui suas dúvidas e sugestões, que entraremos em contato!</p><div class="col-lg-7 px-0 m-auto d-lg-flex justify-content-center"><div class="form d-flex flex-column position-relative col-lg-7"> <?php echo do_shortcode('[contact-form-7 id="111" title="CONTATO"]'); ?> <!-- <input type="text" placeholder="Nome">
            <input type="text" placeholder="Telefone (DDD)">
            <input type="text" placeholder="E-mail">
            <textarea name="mensagem" id="" cols="20" rows="5" placeholder="Mensagem"></textarea>
            <a href="#">Enviar</a> --></div><div class="box p-5 col-lg-4 d-flex flex-column justify-content-center text-center text-lg-left mx-lg-5"><span class="title">Campinas, SP</span> <a href="https://www.google.com.br/maps/dir//Mosca+Log%C3%ADstica+-+Av.+Ant%C3%B4nio+Boscato,+171+-+TIC,+Campinas+-+SP,+13069-119/@-22.8445264,-47.1539319,17z/data=!4m9!4m8!1m0!1m5!1m1!1s0x94bf5d6503bc34b3:0xd137a60c00fb51d4!2m2!1d-47.152411!2d-22.8452045!3e0" target="_blank"><p>Av. Antônio Buscato. 171<br>CEP: 13069-119</p></a><a href="tel:1937812222"><span class="title">(19) 3781.2222</span></a><p>vendas@mosca.com.br</p><div class="d-flex col-8 col-lg-12 px-0 m-auto m-lg-0 justify-content-between"><a class="color-green" target="_blank" href="https://www.facebook.com/moscalogistica"><i class="fab fa-2x fa-facebook-f"></i></a> <a class="color-green" target="_blank" href="https://www.instagram.com/moscalogistica/"><i class="fab fa-2x fa-instagram"></i></a> <a target="_blank" href="https://www.youtube.com/channel/UCDMdr5YLEC0TeXCeMGF6k0g" class="color-green"><i class="fab fa-2x fa-youtube"></i></a> <a target="_blank" href="https://www.linkedin.com/company/mosca-log%C3%ADstica/?viewAsMember=true" class="color-green"><i class="fab fa-2x fa-linkedin"></i></a></div></div></div></section><!-- <div class="p-4 pyt-lg-0 pb-lg-4 mapa ">
    <div class="row align-items-center m-0">
        <div class="col-xl-5 pl-lg-5">
            <div class="pt-3 px-md-5 pt-md-4 px-3">
                <div id="detail" class="my-3 mx-auto mx-xl-0"></div>
                <h2 class="pb-3 fw-400 text-center text-xl-left color-blue">Cobertura de <b>100%</b><br>no Estado de<br><b>São Paulo</b></h2>
            </div>
            <img class="img-fluid caminhao" id="caminhao" src="<?php echo get_stylesheet_directory_uri() ?>/dist/img/caminhao.png" alt="Caminhão da Mosca">
            <div class="pt-3 px-md-5 pt-md-5 px-3">
                <div class="d-none d-xl-block color-green pl-3 border-left-blue atend">
                    13 unidades <br> <small>de atendimento</small>
                </div>
            </div>
        </div>
        <div class="col-xl-7 d-xl-block margin-center d-none">
            <div class="mapa-desktop">
                <img class="img-fluid" src="<?= get_stylesheet_directory_uri() ?>/dist/img/MAPA-pontilhado.png" alt="">
                <div class="unidade sjrp">
                    <div class="infos">
                        <small>Gerente da filial</small>
                        <b class="nome">Antônio Adão</b>
                        <span class="telefone"><i class="fab fa-whatsapp"></i> 17 3388.0530</span>
                        <div class="mail"><a href="mailto:adao@mosca.com.br"></a>adao@mosca.com.br</div>
                    </div>
                    <div class="local"><i class="fas fa-shopping-cart color-green"></i>S. J. R. Preto</div>
                </div>
                <div class="unidade aracatuba">
                    <div class="infos">
                        <small>Atendimento </small>
                        <b class="nome">Comercial</b>
                        <span class="telefone"><i class="fab fa-whatsapp"></i> 19 3781.2222</span>
                        <div class="mail"><a href="mailto:vendas@mosca.com.br"></a>vendas@mosca.com.br</div>
                    </div>
                    <div class="local"><i class="fas fa-shopping-cart color-green"></i>Araçatuba</div>
                </div>
                <div class="unidade ribpreto">
                    <div class="infos">
                        <small>Gerente da filial</small>
                        <b class="nome">Márcio Rezende</b>
                        <span class="telefone"><i class="fab fa-whatsapp"></i> 16 3626.4192</span>
                        <div class="mail"><a href="mailto:marcio@mosca.com.br"></a>marcio@mosca.com.br</div>
                    </div>
                    <div class="local"><i class="fas fa-shopping-cart color-green"></i>Ribeirão Preto</div>
                </div>
                <div class="unidade tupa">
                    <div class="infos">
                        <small>Atendimento </small>
                        <b class="nome">Comercial </b>
                        <span class="telefone"><i class="fab fa-whatsapp"></i> 19 3781.2222</span>
                        <div class="mail"><a href="mailto:vendas@mosca.com.br"></a>vendas@mosca.com.br</div>
                    </div>
                    <div class="local"><i class="fas fa-shopping-cart color-green"></i>Tupã</div>
                </div>
                <div class="unidade bauru">
                    <div class="infos">
                        <small>Gerente da filial</small>
                        <b class="nome">Carlos Akimoto</b>
                        <span class="telefone"><i class="fab fa-whatsapp"></i> 14 3203.6696</span>
                        <div class="mail"><a href="mailto:filialbru@mosca.com.br"></a>filialbru@mosca.com.br</div>
                    </div>
                    <div class="local"><i class="fas fa-shopping-cart color-green"></i>Bauru</div>
                </div>
                <div class="unidade presprudente">
                    <div class="infos">
                        <small>Atendimento</small>
                        <b class="nome">Comercial</b>
                        <span class="telefone"><i class="fab fa-whatsapp"></i> 19 3781.2222</span>
                        <div class="mail"><a href="mailto:vendas@mosca.com.br"></a>vendas@mosca.com.br</div>
                    </div>
                    <div class="local"><i class="fas fa-shopping-cart color-green"></i>Presidente<br> Prudente</div>
                </div>
                <div class="unidade assis">
                    <div class="infos">
                        <small>Atendimento</small>
                        <b class="nome">Comercial</b>
                        <span class="telefone"><i class="fab fa-whatsapp"></i> 19 3781.2222</span>
                        <div class="mail"><a href="mailto:vendas@mosca.com.br"></a>vendas@mosca.com.br</div>
                    </div>
                    <div class="local"><i class="fas fa-shopping-cart color-green"></i>Assis</div>
                </div>
                <div class="unidade itapetininga">
                    <div class="infos">
                        <small>Atendimento</small>
                        <b class="nome">Comercial</b>
                        <span class="telefone"><i class="fab fa-whatsapp"></i> 19 3781.2222</span>
                        <div class="mail"><a href="mailto:vendas@mosca.com.br"></a>vendas@mosca.com.br</div>
                    </div>
                    <div class="local"><i class="fas fa-shopping-cart color-green"></i>Itapetininga</div>
                </div>
                <div class="unidade campinas">
                    <div class="infos">
                        <small>Atendimento</small>
                        <b class="nome">Comercial</b>
                        <span class="telefone"><i class="fab fa-whatsapp"></i> 19 3781.2222</span>
                        <div class="mail"><a href="mailto:vendas@mosca.com.br"></a>vendas@mosca.com.br</div>
                    </div>
                    <div class="local">Campinas <br> <i class="fas fa-shopping-cart color-green"></i></div>
                </div>
                <div class="unidade sjc">
                    <div class="infos">
                        <small>Gerente da filial</small>
                        <b class="nome">Fernanda Gomes</b>
                        <span class="telefone"><i class="fab fa-whatsapp"></i> 12 3939.8523</span>
                        <div class="mail"><a href="mailto:filialsjc@mosca.com.br"></a>filialsjc@mosca.com.br</div>
                    </div>
                    <div class="local"><i class="fas fa-shopping-cart color-green"></i>S. J. dos Campos</div>
                </div>
                <div class="unidade sp">
                    <div class="infos">
                        <small>Gerente da filial</small>
                        <b class="nome">Márcio Leal</b>
                        <span class="telefone"><i class="fab fa-whatsapp"></i> 11 2484.5331 </span>
                        <div class="mail"><a href="mailto:admgru@mosca.com.br"></a>admgru@mosca.com.br</div>
                    </div>
                    <div class="local"><i class="fas fa-shopping-cart color-green"></i>Guarulhos</div>
                </div>
                <div class="unidade santos">
                    <div class="infos">
                        <small>Atendimento</small>
                        <b class="nome">Comercial</b>
                        <span class="telefone"><i class="fab fa-whatsapp"></i> 19 3781.2222</span>
                        <div class="mail"><a href="mailto:vendas@mosca.com.br"></a>vendas@mosca.com.br</div>
                    </div>
                    <div class="local"><i class="fas fa-shopping-cart color-green"></i>Santos</div>
                </div>
                <div class="unidade registro">
                    <div class="infos">
                        <small>Atendimento</small>
                        <b class="nome">Comercial</b>
                        <span class="telefone"><i class="fab fa-whatsapp"></i> 19 3781.2222</span>
                        <div class="mail"><a href="mailto:vendas@mosca.com.br"></a>vendas@mosca.com.br</div>
                    </div>
                    <div class="local">Registro <i class="fas fa-shopping-cart color-green"></i></div>
                </div>
            </div>

        </div>

        <img class="d-xl-none margin-center img-fluid d-block" src="<?= get_stylesheet_directory_uri() ?>/dist/img/MAPA-pontilhado-mobile.png" alt="">
        <div class="d-xl-none d-block text-center col-12 p-0">
            <div id="detail" class="my-3 mx-auto"></div>
            <div style="    height: 70px;"><b>13 unidades</b><br> de atendimento</div>
            <a href="<?php echo get_site_url() ?>/unidades" class="btn-cta">Confira o contato das unidades</a>
        </div>
    </div>
</div> --><div class="p-4 pyt-lg-0 pb-lg-4 mapa bg-gray"><div class="row align-items-center m-0"><div class="col-xl-5"><div class="pt-3 px-md-5 ml-lg-5 pt-md-4 px-3"><div id="detail" class="my-3 mx-auto mx-xl-0"></div><h2 class="pb-3 fw-400 text-center text-xl-left color-blue">Cobertura de <b>100%</b><br>no Estado de<br><b>São Paulo</b></h2></div><img class="img-fluid caminhao" id="caminhao" src="<?php echo get_stylesheet_directory_uri() ?>/dist/img/caminhao.png" alt="Caminhão da Mosca"><div class="pt-3 px-md-5 ml-lg-5 pt-md-5 px-3"><div class="d-none d-xl-block color-green pl-3 border-left-blue atend">16 unidades<br><small>de atendimento</small></div></div></div><div class="col-xl-7 d-xl-block margin-center d-none"><div class="mapa-desktop"><img class="img-fluid" src="<?= get_stylesheet_directory_uri() ?>/dist/img/mapa.png" alt=""><div class="unidade sjrp"><div class="infos"><small>Responsável da filial</small> <b class="nome">Lucas</b> <span class="telefone"><i class="fab fa-whatsapp"></i> (17) 98225-2006</span><div class="mail"><a href="mailto:filialsrp@mosca.com.br"></a>filialsrp@mosca.com.br</div></div><div class="local"><div class="carrinho"><img class="img-fluid" src="<?= get_stylesheet_directory_uri() ?>/dist/img/carrinho.png" alt=""></div>S. J. R. Preto</div></div><div class="unidade marilia"><div class="infos"><small>Atendimento</small> <b class="nome">Lucas</b> <span class="telefone"><i class="fab fa-whatsapp"></i> (14) 3434-4835</span><div class="mail"><a href="mailto:filialmii@mosca.com.br"></a>filialmii@mosca.com.br</div></div><div class="local">Marília<div class="carrinho"><img class="img-fluid" src="<?= get_stylesheet_directory_uri() ?>/dist/img/carrinho.png" alt=""></div></div></div><div class="unidade aracatuba"><div class="infos"><small>Atendimento </small><b class="nome">Ed. Carlos</b> <span class="telefone"><i class="fab fa-whatsapp"></i> (18) 98193-0075</span><div class="mail"><a href="mailto:filialarc@mosca.com.br"></a>filialarc@mosca.com.br</div></div><div class="local"><div class="carrinho"><img class="img-fluid" src="<?= get_stylesheet_directory_uri() ?>/dist/img/carrinho.png" alt=""></div>Araçatuba</div></div><div class="unidade ribpreto"><div class="infos"><small>Responsável da filial</small> <b class="nome">Peterson</b> <span class="telefone"><i class="fab fa-whatsapp"></i> (16) 99275-2379</span><div class="mail"><a href="mailto:filialrpo02@mosca.com.br"></a>filialrpo02@mosca.com.br</div></div><div class="local"><div class="carrinho"><img class="img-fluid" src="<?= get_stylesheet_directory_uri() ?>/dist/img/carrinho.png" alt=""></div>Ribeirão Preto</div></div><div class="unidade sorocaba"><div class="infos"><small>Atendimento </small><b class="nome">Ariane </b><span class="telefone"><i class="fab fa-whatsapp"></i> (19) 98365-0078</span><div class="mail"><a href="mailto:torre@mosca.com.br"></a>torre@mosca.com.br</div></div><div class="local"><div class="carrinho"><img class="img-fluid" src="<?= get_stylesheet_directory_uri() ?>/dist/img/carrinho.png" alt=""></div>Sorocaba</div></div><div class="unidade bauru"><div class="infos"><small>Responsável da filial</small> <b class="nome">Carlos</b> <span class="telefone"><i class="fab fa-whatsapp"></i> (14) 99172-2977</span><div class="mail"><a href="mailto:filialbru@mosca.com.br"></a>filialbru@mosca.com.br</div></div><div class="local"><div class="carrinho"><img class="img-fluid" src="<?= get_stylesheet_directory_uri() ?>/dist/img/carrinho.png" alt=""></div>Bauru</div></div><div class="unidade tatui"><div class="infos"><small>Atendimento </small><b class="nome">Ariane </b><span class="telefone"><i class="fab fa-whatsapp"></i> (19) 98365-0078</span><div class="mail"><a href="mailto:torre@mosca.com.br"></a>torre@mosca.com.br</div></div><div class="local"><div class="carrinho"><img class="img-fluid" src="<?= get_stylesheet_directory_uri() ?>/dist/img/carrinho.png" alt=""></div>Tatuí</div></div><div class="unidade presprudente"><div class="infos"><small>Atendimento</small> <b class="nome">Sergio</b> <span class="telefone"><i class="fab fa-whatsapp"></i> (18) 98147-0284</span><div class="mail"><a href="mailto:filialppe@mosca.com.br"></a>filialppe@mosca.com.br</div></div><div class="local"><div class="carrinho"><img class="img-fluid" src="<?= get_stylesheet_directory_uri() ?>/dist/img/carrinho.png" alt=""></div>Presidente<br>Prudente</div></div><div class="unidade assis"><div class="infos"><small>Atendimento </small><b class="nome">Ariane </b><span class="telefone"><i class="fab fa-whatsapp"></i> (19) 98365-0078</span><div class="mail"><a href="mailto:torre@mosca.com.br"></a>torre@mosca.com.br</div></div><div class="local"><div class="carrinho"><img class="img-fluid" src="<?= get_stylesheet_directory_uri() ?>/dist/img/carrinho.png" alt=""></div>Assis</div></div><div class="unidade itapetininga"><div class="infos"><small>Atendimento </small><b class="nome">Ariane </b><span class="telefone"><i class="fab fa-whatsapp"></i> (19) 98365-0078</span><div class="mail"><a href="mailto:torre@mosca.com.br"></a>torre@mosca.com.br</div></div><div class="local"><div class="carrinho"><img class="img-fluid" src="<?= get_stylesheet_directory_uri() ?>/dist/img/carrinho.png" alt=""></div>Itapetininga</div></div><div class="unidade campinas"><div class="infos"><small>Atendimento </small><b class="nome">Ariane </b><span class="telefone"><i class="fab fa-whatsapp"></i> (19) 98365-0078</span><div class="mail"><a href="mailto:torre@mosca.com.br"></a>torre@mosca.com.br</div></div><div class="local">Campinas<br><div class="carrinho"><img class="img-fluid" src="<?= get_stylesheet_directory_uri() ?>/dist/img/carrinho.png" alt=""></div></div></div><div class="unidade sjc"><div class="infos"><small>Responsável da filial</small> <b class="nome">Yasmim</b> <span class="telefone"><i class="fab fa-whatsapp"></i>(12) 99199-6528</span><div class="mail"><a href="mailto:filialsjc02@mosca.com.br"></a>filialsjc02@mosca.com.br</div></div><div class="local"><div class="carrinho"><img class="img-fluid" src="<?= get_stylesheet_directory_uri() ?>/dist/img/carrinho.png" alt=""></div>S. J. dos Campos</div></div><div class="unidade sp"><div class="infos"><small>Responsável da filial</small> <b class="nome">Erick</b> <span class="telefone"><i class="fab fa-whatsapp"></i> (11) 98989-4169</span><div class="mail"><a href="mailto:filialgru@mosca.com.br"></a>filialgru@mosca.com.br</div></div><div class="local"><div class="carrinho"><img class="img-fluid" src="<?= get_stylesheet_directory_uri() ?>/dist/img/carrinho.png" alt=""></div>Guarulhos</div></div><div class="unidade santos"><div class="infos"><small>Atendimento </small><b class="nome">Ariane </b><span class="telefone"><i class="fab fa-whatsapp"></i> (19) 98365-0078</span><div class="mail"><a href="mailto:torre@mosca.com.br"></a>torre@mosca.com.br</div></div><div class="local"><div class="carrinho"><img class="img-fluid" src="<?= get_stylesheet_directory_uri() ?>/dist/img/carrinho.png" alt=""></div>Santos</div></div><div class="unidade registro"><div class="infos"><small>Atendimento </small><b class="nome">Ariane </b><span class="telefone"><i class="fab fa-whatsapp"></i> (19) 98365-0078</span><div class="mail"><a href="mailto:torre@mosca.com.br"></a>torre@mosca.com.br</div></div><div class="local">Registro<div class="carrinho"><img class="img-fluid" src="<?= get_stylesheet_directory_uri() ?>/dist/img/carrinho.png" alt=""></div></div></div><div class="unidade curitiba"><div class="infos"><small>Atendimento </small><b class="nome">Ariane </b><span class="telefone"><i class="fab fa-whatsapp"></i> (19) 98365-0078</span><div class="mail"><a href="mailto:torre@mosca.com.br"></a>torre@mosca.com.br</div></div><div class="local">Curitiba<div class="carrinho"><img class="img-fluid" src="<?= get_stylesheet_directory_uri() ?>/dist/img/carrinho.png" alt=""></div></div></div></div></div><img class="d-xl-none margin-center img-fluid d-block" src="<?= get_stylesheet_directory_uri() ?>/dist/img/MAPA-pontilhado-mobile.png" alt=""><div class="d-xl-none d-block text-center col-12 p-0"><div id="detail" class="my-3 mx-auto"></div><div style="height: 70px;"><b>16 unidades</b><br>de atendimento</div><a href="<?php echo get_site_url() ?>/unidades" class="btn-cta">Confira o contato das unidades</a></div></div></div><section class="bg-gray"><div class="text-center p-5 py-lg-4 mx-3 d-flex flex-column flex-md-row align-items-center justify-content-between margin-center col-lg-8 banner-fale-home"><h2 class="fw-400 col-md-5 pb-lg-0 mb-lg-0 col-lg-5 color-text pb-3">Quer ver tudo o que <span>podemos</span> fazer por você?</h2><a class="fale" href="tel:1937812222"><img class="" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icone-tel.svg" alt=""><h4>Fale conosco</h4></a><!-- <a class="whats-btn mt-5 mt-md-0" href="#">
            <i class="fab fa-whatsapp fa-2x text-white"></i>
        </a> --></div></section> <?php get_template_part('midias'); ?> <?php get_footer(); ?>