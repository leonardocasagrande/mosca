<?php get_header(); ?>


<div class="banner  bg-denuncia">
  <div class="filtro">
    <div class="p-150">
      <div id="detail"></div>
      <h1 class="text-white ">Canal de Ética Mosca Logística</h1>
      <p>Um espaço seguro e confidencial<br> para promover a transparência<br>e a integridade.</p>
      <a href="#denuncia" class="cta-banner"> Fazer denúncia </a>
    </div>
  </div>

  
</div>

<section class="etica">
    <div class="conteudo-etica">
    <img src="<?php echo get_stylesheet_directory_uri() ?>/dist/img/carinha1.png" alt="" class="carinha d-none d-lg-block">

        <div class="row justify-content-between m-0">
            <div class="col-lg-3 coluna-etica">
                <h2>Ética e Contuda</h2>
                <p class="d-lg-block d-none">Na Mosca Logística, acreditamos em um ambiente ético e transparente.<br>
                O Canal de Ética é uma<br>ferramenta segura e confiável<br>para relatar irregularidades e contribuir para
                um <br>local de trabalho melhor
                e mais justo.</p>
                <p class="d-lg-none">Na Mosca Logística, acreditamos em um ambiente ético e transparente.<br>
                O Canal de Ética é uma ferramenta segura e confiável para relatar irregularidades e contribuir para
                um local de trabalho melhor
                e mais justo.</p>
            </div>
            <div class="d-lg-none">
                <img src="<?php echo get_stylesheet_directory_uri() ?>/dist/img/carinha1.png" alt="" class="img-fluid carinha">
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-6 coluna-etica col-verde">
                <h2>O Que é o Canal de Denúncias?</h2>
                <p>O Canal de Denúncias é um instrumento seguro e confidencial para colaboradores, parceiros, fornecedores, clientes e comunidade reportarem comportamentos inadequados ou irregularidades. Alinhado às nossas políticas de ética e conduta, ele oferece um meio seguro para relatar casos, com a garantia de que suas informações serão tratadas com sigilo e responsabilidade.</p>
                <hr class="detalhe">
            </div>
        </div>
    </div>
</section>
<section class="oqdenunciar">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="row align-items-center">
                    <div class="col-lg-3">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/dist/img/correto.png" alt="" class="img-fluid d-none d-lg-block">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/dist/img/correto-mobile.png" alt="" class="img-fluid d-lg-none">

                    </div>
                    <div class="col-lg-9">
                        <h2>O que denunciar?</h2>
                        <ul class="d-flex flex-wrap d-lg-block">
                            <li class="col-6 mt-2">• Assédio</li>
                            <li class="col-6 mt-2">• Roubo</li>
                            <li class="col-12 mt-2">• Comportamentos Antiéticos</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 mt-4 mt-lg-0">
                <div class="row align-items-center">
                    <div class="col-lg-3">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/dist/img/errado.png" alt="" class="img-fluid d-none d-lg-block">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/dist/img/errado-mobile.png" alt="" class="img-fluid d-lg-none">

                    </div>
                    <div class="col-lg-9">
                        <h2>O que <b>NÃO</b> denunciar?</h2>
                        <ul class="d-flex justify-content-center d-lg-block">
                            <li class="pe-4 mt-lg-2">• Fofocas</li>
                            <li class="pe-4 mt-lg-2">• Intrigas</li>
                            <li class=" mt-lg-2">• Reclamações</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="comorelatar">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-4">
                <h2>Como relatar<br>uma denúncia?</h2>
                <p>Todos os relatos são tratados com total sigilo, e o denunciante tem a opção de permanecer anônimo. Estamos comprometidos em garantir a segurança e a privacidade de quem utiliza o nosso Canal de Ética.</p>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-6">
                <img src="<?php echo get_stylesheet_directory_uri() ?>/dist/img/maozinhas.png" alt="" class="img-fluid">

            </div>
        </div>
    </div>
</section>
<section class="links" id="denuncia">
    <div class="container-fluid row p-lg-0">
        <div class="col-lg-2 p-0">
            <div class="facadenuncia">
                <span class="col-12">Faça sua denúncia</span>
                <hr class="efeito">
            </div>
        </div>
        <div class="col-lg-8 info-links">
            <div class="row align-items-center mb-lg-4">
                <div class="col-lg-8">
                    <h2>Via Site</h2>
                    <p>Acesse nosso portal e responda a algumas perguntas para registrar sua denúncia</p>
                </div>
                <div class="col-lg-4 text-center">
                    <a href="https://canal.ouvidordigital.com.br/mosca/form?lang=pt" class="btn-cta btn-azul">Relate pela web</a>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8">
                    <h2>Via Whatsapp</h2>
                    <p><b>Envie uma mensagem para o número abaixo ou use o QR Code.</b> No primeiro contato, chegará uma mensagem automática solicitando o código da empresa, que deverá ser informado (MOSCA). Você pode optar por enviar um áudio explicando o acontecimento.</p>
                    <p><b>Dica: Anote seu número de protocolo para acompanhar o andamento da denúncia.</b></p>
                </div>
                <div class="col-lg-4 text-center">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/dist/img/qrcode.png" alt="" class="img-fluid">

                    <a href="https://api.whatsapp.com/send/?phone=+553189477889&text=Quero+fazer+uma+den%C3%BAncia+para+a+empresa+mosca" class="btn-cta ">Relate pelo Whatsapp</a>
                </div>
            </div>
            <div class="aviso-verde">
                <p>Esse é um portal seguro e independente para registro de informações de violação de condutas éticas
                ou descumprimento das legislações, as informações serão recebidas pela empresa Ouvidor Digital, sendo independente e especializada no assunto, juntamente com o Comitê de Ética Mosca.</p>
                <p><b>Garantimos o anonimato e segurança da identidade do denunciante.</b></p>
            </div>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
</section>


<?php get_footer(); ?>