<!DOCTYPE html>

<html lang="pt_BR">

<head>
	<!-- Google Tag Manager -->
	<script>
		(function(w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({
				'gtm.start': new Date().getTime(),
				event: 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0],
				j = d.createElement(s),
				dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', 'GTM-TXBM9GQ');
	</script>
	<!-- End Google Tag Manager -->

	<meta charset="UTF-8">

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>

		Mosca Logística <?php wp_title(); ?>

	</title>

	<meta name="robots" content="index, follow" />

	<meta name="msapplication-TileColor" content="#ffffff">

	<meta name="theme-color" content="#ffffff">
	<link href="https://fonts.googleapis.com/css?family=Barlow&display=swap" rel="stylesheet">


	<!-- <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" /> -->

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/tiny-slider.css">

	<link rel="stylesheet" type="text/css" href="<?= get_stylesheet_directory_uri(); ?>/dist/css/style.css">


	<?php wp_head(); ?>
</head>

<body>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TXBM9GQ" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<header class="menu-fixo">
		<div class="sob-menu">
			
			<button type="button" data-toggle="modal" data-target="#exampleModal" class="btn-trabalhe">Trabalhe Conosco</button>
			
		</div>

		<nav class="navbar  p-0 navbar-expand-lg  align-items-center">
			<div class=" p-3 p-lg-0 d-flex justify-content-between  align-items-center w-100">
				<a href="<?php echo get_site_url(); ?>/">
					<div class="logo-site ml-md-0"> </div>
				</a>



				<div class="collapse  navbar-collapse-sm d-lg-flex justify-content-center " id="navbarNavDropdown">

					<ul class="navbar-nav pl-md-3 ">

						<li class="nav-item ">
							<a class="nav-link" href="<?php echo get_site_url() ?>/sobre">Sobre</a>
						</li>

						<!-- <li id="btn-seg" class="nav-item ">
							<a class="nav-link click-seg" href="#">Segmentos </a>
						</li> -->

						<li class="nav-item">
							<a class="nav-link" href="<?php echo get_site_url(); ?>/diferenciais"> Diferenciais</a>
						</li>

						<!-- <li id="btn-unt" class="nav-item">
							<a id="unity" class="nav-link" href="#">Unidades</a>
						</li> -->

						<li class="nav-item">
							<a class="nav-link" href="<?php echo get_site_url(); ?>/sustentabilidade">Sustentabilidade</a>
						</li>

						<li class="nav-item ">
							<a class="nav-link" href="<?php echo get_site_url() ?>/certificacoes">Certificações </a>
						</li>

						<li class="nav-item ">
							<a class="nav-link" href="<?php echo get_site_url(); ?>/contato">Contato</a>
						</li>
						<li class="nav-item ">
							<a class="nav-link" href="<?php echo get_site_url(); ?>/canal-de-etica">Canal de Ética</a>
						</li>
						<!-- <li class="nav-item ">
							<a class="nav-link" href="<?php echo get_site_url(); ?>/rastreie-sua-carga" target="_blank">Rastreie sua Carga<img class="pl-3 d-md-inline-block d-none" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/Search.png"></a>
						</li> -->

					</ul>
				</div>

				<button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
				</button>

			</div>

		</nav>


	</header>