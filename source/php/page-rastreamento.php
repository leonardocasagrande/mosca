<script language=JavaScript>
    function Limpar1() {
        document.form1.reset();
        document.form1.NR.focus();
    }

    function Vai1() {
        if (document.form1.cnpj.value == "") {
            alert("Informe o CNPJ do remetente.");
            return;
        }
        if (document.form1.NR.value == "") {
            alert("Informe pelo menos uma Nota Fiscal.");
            return;
        }
        document.form1.submit();
    }
</script>

<form name=form1 action=https://ssw.inf.br/2/ssw_resultSSW method=post>

    <table cellSpacing=2 cellPadding=2 width="100%" border=0>
        <tr>
            <td width=200>Notas Fiscais:</TD>
            <td>
                <textarea name=NR rows=10 cols=30></textarea>
            </td>
        </tr>
        <tr>
            <td>CNPJ do Remetente:</td>
            <td><INPUT maxLength=14 name=cnpj></td>
        </tr>
        <tr>
            <td>Senha:</td>
            <td><INPUT type="password" name="chave"></td>
        </tr>
        <tr>
            <td> </td>
            <td>
                <a href="javascript:Vai1()">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/enviar.gif" border=0></a>
                <a href="javascript:Limpar1()">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/limpar.gif" border=0></a>
            </td>
        </tr>

        <input type="hidden" name="urlori" value="https://mosca.com.br/rastreamento">
    </table>

</form>