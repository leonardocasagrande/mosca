<?php the_post();
get_header(); ?>

<div class="banner-home">

  <div class="banner-carousel banner-home d-none d-lg-flex">


    <div class="banner-item bg-0">
    <a href="<?php echo get_site_url(); ?>/contato" target="_blank"></a>
      <div class="filtro-home filtro-off">
        <!-- <div class="p-150">
          <div id="detail"></div>
          <h1 class="text-white">A sua logística até o varejo em boas mãos</h1>
          <p class="text-white">Conheça o melhor caminho da indústria até o varejo.</p>

          <div class="circle">
            <i class="text-white fas fa-arrow-down"></i>
          </div>
        </div> -->
      </div>
    </div>

    <div class="banner-item bg-1">
      <div class="filtro-home">
        <div class="p-150">
          <div id="detail"></div>
          <h1 class="text-white">A sua logística até o varejo em boas mãos</h1>
          <p class="text-white">Conheça o melhor caminho da indústria até o varejo.</p>

          <div class="circle">
            <i class="text-white fas fa-arrow-down"></i>
          </div>
        </div>
      </div>
    </div>



    <div class="banner-item bg-2 ">
      <div class="filtro-home">
        <div class="p-150">
          <div id="detail"></div>
          <h1 class="text-white">A sua logística até o varejo em boas mãos</h1>
          <p class="text-white">Conheça o melhor caminho da indústria até o varejo.</p>

        </div>
      </div>
    </div>


    <div class="banner-item bg-3 ">
      <div class="filtro-home filtro-off">
        <div class="p-150">
          <div id="detail"></div>
          <h1 class="text-white">A sua logística até o varejo em boas mãos</h1>
          <p class="text-white">Conheça o melhor caminho da indústria até o varejo.</p>


        </div>
      </div>
    </div>

    <div class="banner-item bg-4 ">
      <a href="<?php echo get_site_url(); ?>/sustentabilidade" target="_blank"></a>
      <div class="filtro-home filtro-off">
        <!-- <div class="p-150">
          <div id="detail"></div>
          <h1 class="text-white">A sua logística até o varejo em boas mãos</h1>
          <p class="text-white">Conheça o melhor caminho da indústria até o varejo.</p> 


        </div> -->
      </div>
    </div>

    <div class="banner-item bg-5 ">
      <a href="mailto:comercial@mosca.com.br" target="_blank"></a>
      <div class="filtro-home filtro-off">
        <!-- <div class="p-150">
          <div id="detail"></div>
          <h1 class="text-white">A sua logística até o varejo em boas mãos</h1>
          <p class="text-white">Conheça o melhor caminho da indústria até o varejo.</p> 


        </div> -->
      </div>
    </div>

  </div>

  <div class="banner-carousel banner-home d-block d-lg-none">

    <div class="banner-item bg-1">
      <div class="filtro-home">
        <div class="p-150">
          <div id="detail"></div>
          <h1 class="text-white">A sua logística até o varejo em boas mãos</h1>
          <p class="text-white">Conheça o melhor caminho da indústria até o varejo.</p>

          <div class="circle">
            <i class="text-white fas fa-arrow-down"></i>
          </div>
        </div>
      </div>
    </div>

    <div class="banner-item bg-0">
      
      <div class="filtro-home filtro-off">
        <!-- <div class="p-150">
          <div id="detail"></div>
          <h1 class="text-white">A sua logística até o varejo em boas mãos</h1>
          <p class="text-white">Conheça o melhor caminho da indústria até o varejo.</p>

          <div class="circle">
            <i class="text-white fas fa-arrow-down"></i>
          </div>
        </div> -->
      </div>
    </div>
  </div>


  <a href="#" class="circle d-md-none c-detail">
    <i class="fas fa-arrow-up"></i>
  </a>

</div>
<!-- <div class="banner-natal">
  <img class="img-fluid" src="<?= get_stylesheet_directory_uri() ?>/dist/img/bg-home-5-lg.jpg" alt="Feliz Natal! e um incrivel Ano Novo de toda a equipe da mosca">
</div> -->

<div class="bg-lg-white pb-5 pb-lg-0 carosel-contaier">
  <div id="teste" class="   d-flex justify-content-center flex-lg-row">

    <a class="item col-lg-4" href="<?php echo get_site_url() ?>/rastreamento" target="_blank">
      <div class="bg-green button-home text-lg-center  p-md-4 d-lg-flex flex-column flex-lg-row align-items-center text-white ">
        <img class="" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/lupa.svg" alt="">
        <h2>Rastreie sua carga</h2>
      </div>
    </a>

    <a class="item col-lg-4" href="https://ssw.inf.br/2/servico?sc=N&sigla_emp=MOL" target="_blank">
      <div class="bg-blue button-home text-lg-center p-md-4 d-lg-flex align-items-center   ">
        <img class="" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cliente.svg" alt="">
        <h2 class="text-white">Área do Cliente</h2>
      </div>
    </a>

    <!-- Button trigger modal -->
    <!-- <a href="" type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal2">
      Launch demo modal
    </a> -->



    <a class="item col-lg-4 " href="tel:1937812222">
      <div class="bg-gray bg-desk-gray button-home text-lg-center p-md-4 d-lg-flex align-items-center text-white">
        <img class="" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/telefone.svg" alt="">
        <h2 class="color-blue">Fale Conosco</h2>
      </div>
    </a>

  </div>
</div>


<div class="">
  <div class="bg-white position-relative pb-5 pt-lg-5 desk-home d-md-flex justify-content-end align-items-center">

    <div class="gradient h-100 blue-bg pl-lg-4 col-xl-4  col-lg-5 pb-4">

      <div class="d-none d-desk d-md-block"></div>
      <h1 class="col-xl-9 p-0 px-0 pb-3 color-desk">Logística especializada em varejo no Estado de São Paulo
      </h1>
      <div class="d-md-none"></div>
      <p class="col-lg-8 p-0 color-desk">Somos especializados em logística fracionada para o abastecimento do segmento supermercadista.</p>
      <a href="<?php echo get_site_url() ?>/sobre">
        <span>Saiba mais</span>
        <i class="fas fa-arrow-right"></i>
      </a>
    </div>

    <div class="home-2   col-md-6 col-xl-7 px-0 col-lg-6"></div>

  </div>
</div>





<section class="bg-gray seg">

  <div class=" d-md-flex pb-4 flex-column align-items-center">

    <div class="pt-3 px-md-5 pt-md-4 px-3">
      <h1 class="pb-3 fw-400 text-center color-text ">Segmentos de atuação</h1>
    </div>

    <div class=" seg-desk d-lg-none">

      <?php $argsSegmentos = array(
        'post_type' => 'segmentos',
        'posts_per_page' => -1
      );
      $segmentos = new WP_Query($argsSegmentos);
      //  var_dump($segmentos);
      if ($segmentos->have_posts()) :
        while ($segmentos->have_posts()) : $segmentos->the_post();
      ?>
          <div class="card ">
            <div class="thumb" style="background:url(<?php echo get_the_post_thumbnail_url(); ?>) center center no-repeat"></div>
            <div class="the-line"></div>
            <span class="titulo"><?php the_title(); ?></span>
            <span class="intro"><?= the_excerpt(); ?></span>
            <a href="<?= the_permalink(); ?>" class="btn-saiba">Saiba mais</a>
          </div>

      <?php endwhile;
      endif; ?>


    </div>



  </div>


  <div class="d-lg-flex d-none px-xl-5 m-auto justify-content-around pb-lg-5 col-12 col-xl-11 seg-desk-home">

    <?php $argsSegmentos = array(
      'post_type' => 'segmentos',
      'posts_per_page' => -1
    );
    $segmentos = new WP_Query($argsSegmentos);
    //  var_dump($segmentos);
    if ($segmentos->have_posts()) :
      while ($segmentos->have_posts()) : $segmentos->the_post();
    ?>
        <div class="card">
          <div class="p-3">
            <span class="titulo"><?php the_title(); ?></span>
            <div class="detalhe"></div>
            <span class="text"><?php the_excerpt(); ?></span>
          </div>

          <img class="thumb" src="<?php echo get_the_post_thumbnail_url(); ?>"></img>
          <a href="<?php the_permalink(); ?>">Saiba Mais</a>

        </div>

    <?php endwhile;
    endif; ?>


  </div>



  </div>

</section>





<section class="bg-white ">
  <div class="wrapper-logos">
    <div class="logos-carosel slider d-flex align-items-center py-0">
      <div class="slide-track">
        <?php
        if (have_rows('repetidor_clientes', 184)) : while (have_rows('repetidor_clientes', 184)) : the_row();
        ?>
            <img class="slide" src="<?php echo get_sub_field('logo') ?>" alt="">
        <?php endwhile;
        endif; ?>
      </div>
    </div>
  </div>
</section>


<!-- MAPA INTERATIVO -->

<div class="p-4 pyt-lg-0 pb-lg-4 mapa bg-gray">
  <div class="row align-items-center m-0">
    <div class="col-xl-5">
      <div class="pt-3 px-md-5 ml-lg-5 pt-md-4 px-3">
        <div id="detail" class="my-3 mx-auto mx-xl-0"></div>
        <h2 class="pb-3 fw-400 text-center text-xl-left color-blue">Cobertura de <b>100%</b><br>no Estado de<br><b>São Paulo</b></h2>
      </div>
      <img class="img-fluid caminhao" id="caminhao" src="<?php echo get_stylesheet_directory_uri() ?>/dist/img/caminhao.png" alt="Caminhão da Mosca">
      <div class="pt-3 px-md-5 ml-lg-5 pt-md-5 px-3">
        <div class="d-none d-xl-block color-green pl-3 border-left-blue atend">
          15 unidades <br> <small>de atendimento</small>
        </div>
      </div>
    </div>
    <div class="col-xl-7 d-xl-block margin-center d-none">
      <div class="mapa-desktop">
        <img class="img-fluid" src="<?= get_stylesheet_directory_uri() ?>/dist/img/MAPA-pontilhado.png" alt="">
        <div class="unidade sjrp">
          <div class="infos">
            <small>Responsável da filial</small>
            <b class="nome">Fernanda Azevedo</b>
            <span class="telefone"><i class="fab fa-whatsapp"></i> 17 3388.0530</span>
            <div class="mail"><a href="mailto:admsrp@mosca.com.br"></a>admsrp@mosca.com.br</div>
          </div>
          <div class="local"><i class="fas fa-shopping-cart color-green"></i> S. J. R. Preto</div>
        </div>
        <div class="unidade aracatuba">
          <div class="infos">
            <small>Atendimento </small>
            <b class="nome">Denise Freitas</b>
            <span class="telefone"><i class="fab fa-whatsapp"></i> 18 98193.0075</span>
            <div class="mail"><a href="mailto:vendas@mosca.com.br"></a>vendas@mosca.com.br</div>
          </div>
          <div class="local"><i class="fas fa-shopping-cart color-green"></i>Araçatuba</div>
        </div>
        <div class="unidade ribpreto">
          <div class="infos">
            <small>Responsável da filial</small>
            <b class="nome">Márcio Rezende</b>
            <span class="telefone"><i class="fab fa-whatsapp"></i> 16 3626.4192</span>
            <div class="mail"><a href="mailto:marcio@mosca.com.br"></a>marcio@mosca.com.br</div>
          </div>
          <div class="local"><i class="fas fa-shopping-cart color-green"></i>Ribeirão Preto</div>
        </div>
        <div class="unidade tupa">
          <div class="infos">
            <small>Atendimento </small>
            <b class="nome">Jéssica Garcia </b>
            <span class="telefone"><i class="fab fa-whatsapp"></i> 19 3781.2222</span>
            <div class="mail"><a href="mailto:vendas@mosca.com.br"></a>vendas@mosca.com.br</div>
          </div>
          <div class="local"><i class="fas fa-shopping-cart color-green"></i>Tupã</div>
        </div>
        <div class="unidade bauru">
          <div class="infos">
            <small>Responsável da filial</small>
            <b class="nome">Carlos Akimoto</b>
            <span class="telefone"><i class="fab fa-whatsapp"></i> 14 3203.6696</span>
            <div class="mail"><a href="mailto:filialbru@mosca.com.br"></a>filialbru@mosca.com.br</div>
          </div>
          <div class="local"><i class="fas fa-shopping-cart color-green"></i>Bauru</div>
        </div>
        <div class="unidade presprudente">
          <div class="infos">
            <small>Atendimento</small>
            <b class="nome">Jéssica Garcia</b>
            <span class="telefone"><i class="fab fa-whatsapp"></i> 19 3781.2222</span>
            <div class="mail"><a href="mailto:vendas@mosca.com.br"></a>vendas@mosca.com.br</div>
          </div>
          <div class="local"><i class="fas fa-shopping-cart color-green"></i>Presidente<br> Prudente</div>
        </div>
        <div class="unidade assis">
          <div class="infos">
            <small>Atendimento</small>
            <b class="nome">Jéssica Garcia</b>
            <span class="telefone"><i class="fab fa-whatsapp"></i> 19 3781.2222</span>
            <div class="mail"><a href="mailto:vendas@mosca.com.br"></a>vendas@mosca.com.br</div>
          </div>
          <div class="local"><i class="fas fa-shopping-cart color-green"></i>Assis</div>
        </div>
        <div class="unidade itapetininga">
          <div class="infos">
            <small>Atendimento</small>
            <b class="nome">Jéssica Garcia</b>
            <span class="telefone"><i class="fab fa-whatsapp"></i> 19 3781.2222</span>
            <div class="mail"><a href="mailto:vendas@mosca.com.br"></a>vendas@mosca.com.br</div>
          </div>
          <div class="local"><i class="fas fa-shopping-cart color-green"></i>Itapetininga</div>
        </div>
        <div class="unidade campinas">
          <div class="infos">
            <small>Atendimento</small>
            <b class="nome">Jéssica Garcia</b>
            <span class="telefone"><i class="fab fa-whatsapp"></i> 19 3781.2222</span>
            <div class="mail"><a href="mailto:vendas@mosca.com.br"></a>vendas@mosca.com.br</div>
          </div>
          <div class="local">Campinas <br> <i class="fas fa-shopping-cart color-green"></i></div>
        </div>
        <div class="unidade sjc">
          <div class="infos">
            <small>Responsável da filial</small>
            <b class="nome">Fernanda Gomes</b>
            <span class="telefone"><i class="fab fa-whatsapp"></i> 12 3939.8523</span>
            <div class="mail"><a href="mailto:filialsjc@mosca.com.br"></a>filialsjc@mosca.com.br</div>
          </div>
          <div class="local"><i class="fas fa-shopping-cart color-green"></i>S. J. dos Campos</div>
        </div>
        <div class="unidade sp">
          <div class="infos">
            <small>Responsável da filial</small>
            <b class="nome">Anderson Oliveira</b>
            <span class="telefone"><i class="fab fa-whatsapp"></i> 11 2484.5331 </span>
            <div class="mail"><a href="mailto:admgru@mosca.com.br"></a>admgru@mosca.com.br</div>
          </div>
          <div class="local"><i class="fas fa-shopping-cart color-green"></i>Guarulhos</div>
        </div>
        <div class="unidade santos">
          <div class="infos">
            <small>Atendimento</small>
            <b class="nome">Jéssica Garcia</b>
            <span class="telefone"><i class="fab fa-whatsapp"></i> 19 3781.2222</span>
            <div class="mail"><a href="mailto:vendas@mosca.com.br"></a>vendas@mosca.com.br</div>
          </div>
          <div class="local"><i class="fas fa-shopping-cart color-green"></i>Santos</div>
        </div>
        <div class="unidade registro">
          <div class="infos">
            <small>Atendimento</small>
            <b class="nome">Jéssica Garcia</b>
            <span class="telefone"><i class="fab fa-whatsapp"></i> 19 3781.2222</span>
            <div class="mail"><a href="mailto:vendas@mosca.com.br"></a>vendas@mosca.com.br</div>
          </div>
          <div class="local">Registro <i class="fas fa-shopping-cart color-green"></i></div>
        </div>
      </div>

    </div>

    <img class="d-xl-none margin-center img-fluid d-block" src="<?= get_stylesheet_directory_uri() ?>/dist/img/MAPA-pontilhado-mobile.png" alt="">
    <div class="d-xl-none d-block text-center col-12 p-0">
      <div id="detail" class="my-3 mx-auto"></div>
      <div style="    height: 70px;"><b>15 unidades</b><br> de atendimento</div>
      <a href="<?php echo get_site_url() ?>/unidades" class="btn-cta">Confira o contato das unidades</a>
    </div>
  </div>
</div>


<section class="depoimentos">
  <span class="titulo">Depoimentos</span>

  <div class="depo-wrapper ">
    <div class="depo-container ">

      <?php $argsDepoimentos = array(
        'post_type' => 'depoimentos',
        'posts_per_page' => -1,
        'tax_query' => array(
          array(
            'taxonomy' => 'categoria_depoimento',
            'field' => 'slug',
            'terms' => 'geral',
          )
        )
      );
      $depoimentos = new WP_Query($argsDepoimentos);
      //  var_dump($segmentos);

      if ($depoimentos->have_posts()) :
        while ($depoimentos->have_posts()) : $depoimentos->the_post();
      ?>

          <div class="depo-item">
            <span class="name"><?php the_title(); ?></span>
            <span class="empresa"><?php the_field('empresa'); ?></span>
            <div class="divisor"></div>
            <div class="excerpt"><?php the_excerpt(); ?>
              <div class="aspas"></div>
            </div>

            <button href="#" type="button" class="btn btn-primary d-none " data-toggle="modal" data-target="#depo-<?php the_id(); ?>">
              Ver

            </button>

          </div>





      <?php endwhile;
      endif; ?>

    </div>
  </div>

</section>



<section class="bg-white">
  <div class="text-center p-5 py-lg-4   mx-3 d-flex flex-column flex-md-row align-items-center justify-content-between margin-center col-lg-7 banner-fale-home">
    <h2 class="fw-400 col-md-5   pb-lg-0 mb-lg-0 col-lg-6 color-text pb-3">
      Quer ver tudo o que <span>podemos</span> fazer por você?
    </h2>

    <a class="fale " href="tel:1937812222">
      <img class="" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icone-tel.svg" alt="">

      <h4>Fale conosco</h4>
    </a>



  </div>
</section>





<?php $argsDepoimentos = array(
  'post_type' => 'depoimentos',
  'posts_per_page' => -1
);
$depoimentos = new WP_Query($argsDepoimentos);
//  var_dump($segmentos);

if ($depoimentos->have_posts()) :
  while ($depoimentos->have_posts()) : $depoimentos->the_post();
?>



    <div class="modal fade" id="depo-<?php the_id(); ?>" tabindex="-1" role="dialog" aria-labelledby="<?php the_id(); ?>" aria-hidden="true">
      <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content ">
          <div class="modal-header">

            <div>
              <h5 class="modal-title name" id="exampleModalLongTitle"><?php the_title(); ?></h5>
              <span class="d-block empresa" id="exampleModalLongTitle"><?php the_field('empresa'); ?></span>
            </div>

            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body conteudo">
            <?php the_content(); ?>
          </div>
          <!-- <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          </div> -->
        </div>
      </div>
    </div>


<?php endwhile;
endif; ?>



<!-- Modal -->
<!-- <div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div> -->



<?php get_template_part('midias'); ?>

<?php get_footer() ?>