<?php get_header(); ?>


<div class="  bg-last-mile">
<img src="<?= get_stylesheet_directory_uri() ?>/dist/img/banner-last-mile.jpg" alt="" class="img-fluid">
</div>


<div class="container py-4">
    <div class="titulo-last">
    <h2 class="">Beneficios</h2>
    <hr class="efeito-verde">
    </div>
</div>


<!-- CARROSSEL -->
<div class="desk-diferenciais py-md-3 pt-lg-0 pt-5 last-mile w-100">



  <div class="text-center  py-4 px-4">

  </div>

  <div class="d-flex  px-2 flex-md-wrap overflow-auto ">

    <div class="box col-3 pr-4 col-md-4 pb-md-3  ">
      <img src="<?= get_stylesheet_directory_uri() ?>/dist/img/certificacoes.png" alt="" class="img-mile">
      <div><p class="titulo-last-box pt-3 pt-md-0">Certificações<br>regulatórias</p>
      </div>
        
    </div>
    <div class="box col-3 pr-4 col-md-4 pb-md-3  ">
      <img src="<?= get_stylesheet_directory_uri() ?>/dist/img/sae.png" alt="" class="img-mile">
      <div><p class="titulo-last-box pt-3 pt-md-0">SA<span>E</span> dedicado<br>a cada <span>E</span>ntrega</p>
      </div>
        
    </div>
    <div class="box col-3 pr-4 col-md-4 pb-md-3  ">
      <img src="<?= get_stylesheet_directory_uri() ?>/dist/img/book.png" alt="" class="img-mile">
      <div><p class="titulo-last-box pt-3 pt-md-0">Book logístico</p>
      <p class="d-none d-md-block">Referência no segmento supermercadista</p></div>
        
    </div>
    <div class="box col-3 pr-4 col-md-4 pb-md-3  ">
      <img src="<?= get_stylesheet_directory_uri() ?>/dist/img/logistica.png" alt="" class="img-mile">
      <div><p class="titulo-last-box pt-3 pt-md-0">Logística<br>reversa</p>
      </div>
        
    </div>
    <div class="box col-3 pr-4 col-md-4 pb-md-3 ">
      <img src="<?= get_stylesheet_directory_uri() ?>/dist/img/seop.png" alt="" class="img-mile">
      <div><p class="titulo-last-box pt-3 pt-md-0 ">S&OP</p>
      <p class="d-none d-md-block">Planejamento de Vendas e Operações</p></div>
        
    </div>
    <div class="box col-3 pr-4 col-md-4 pb-md-3  ">
      <img src="<?= get_stylesheet_directory_uri() ?>/dist/img/customer-exp.png" alt="" class="img-mile">
      <div><p class="titulo-last-box pt-3 pt-md-0">Customer<br>experience</p>
      </div>
        
    </div>
    <div class="box col-3 pr-4 col-md-4 pb-md-3  ">
      <img src="<?= get_stylesheet_directory_uri() ?>/dist/img/excelencia.png" alt="" class="img-mile">
      <div><p class="titulo-last-box pt-3 pt-md-0">Excelência pelo <br> que fazemos</p>
      <p class="d-none d-md-block">Desenvolvimento constante</p></div>
    </div>

    

    <div class="box col-3 pr-4 col-md-4 pb-md-3  ">
      <img src="<?= get_stylesheet_directory_uri() ?>/dist/img/calendarizacao.png" alt="" class="img-mile">
      <div><p class="titulo-last-box pt-3 pt-md-0">Calendarização <br> de entregas</p>
      </div>
        
    </div>

    <div class="box col-3 pr-4 col-md-4 pb-md-3  ">
      <img src="<?= get_stylesheet_directory_uri() ?>/dist/img/api.png" alt="" class="img-mile">
      <div><p class="titulo-last-box pt-3 pt-md-0">API</p>
      <p class="d-none d-md-block">Maior comodidade no tracking</p></div>
        
    </div>


    

    
  </div>

  <div class="position-relative pb-4 d-md-none">
    <div class="arrow arrow-first"></div>
    <div class="arrow arrow-second"></div>
  </div>

</div>

<section class="bg-white">
  <div class="text-center p-5 py-lg-4   mx-3 d-flex flex-column flex-md-row align-items-center justify-content-between margin-center col-lg-7 banner-fale-home">
    <h2 class="fw-400 col-md-5   pb-lg-0 mb-lg-0 col-lg-6 color-text pb-3">Quer saber mais?</h2>
    <a class="fale" href="<?= get_site_url() ?>/contato">
      
      <h4>Entre em contato</h4>
    </a>
  </div>
</section>
<div class="bg-blue pb-0 pb-lg-5 ">

  <div class="p-130 form-apn pt-lg-3 pb-3 text-white text-center">
    <div class="diferencial-detail d-lg-none margin-center"></div>
    <h2 class="col-7titulo-last pt-3 pt-md-0pb-4 px-0 text-white col-lg-4 margin-center">Gostaria de receber nossa
      <p class="d-none d-md-block"></p>
      <span class="color-green"> apresentação
        institucional?<span>
    </h2>

    <!-- <div class="margin-center d-flex flex-column justify-content-md-around flex-lg-row align-items-center col-10 col-lg-7"> -->
    <!-- <input class="row col-12 col-lg-3 mb-3 mb-lg-0 " type="text" placeholder="Nome">
      <input class="row col-12 mb-3 col-lg-2 mb-lg-0" type="text" placeholder="Email">
      <input class="row col-12 mb-4 col-lg-2 mb-lg-0" type="text" placeholder="Empresa">

      <a class="btn-diferenciais " href="<?= get_stylesheet_directory_uri(); ?>/dist/files/apndigital.pdf" target="_blank" download="apndigital.pdf">Confira nossa apresentação</a> -->

    <?php echo do_shortcode('[contact-form-7 id="122" title="APRESENTAÇÃO INSTITUCIONAL"]'); ?>

    <!-- </div> -->
  </div>

</div>

<?php get_template_part('midias'); ?>

<script>
  document.addEventListener('wpcf7mailsent', function(event) {

    window.open("http://mosca.com.br/wp-content/themes/mosca/dist/files/apndigital.pdf");

  }, false);
</script>

<?php get_footer(); ?>