<footer class="bg-footer text-center text-lg-start pt-5 pb-5 ">


	<div class=" d-lg-flex justify-content-between m-auto   col-lg-9 col-xl-8">




		<div class="col-lg-1 px-0 pb-3 pb-lg-0 d-lg-block d-none">
			<img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/top-transporte.png" alt="" class="img-fluid ">
		</div>

		<div class="d-flex ">
			<div class="col-5 col-lg-4  px-3 mr-3 pb-3 pb-lg-0 p-md-0 colunas-footer mr-md-0 ">
				<h2 class="titulo">Menu</h2>
				<ul>
					<li><a href="<?php echo get_site_url() ?>/sobre">Sobre</a></li>
					<li><a href="<?php echo get_site_url() ?>/diferenciais">Diferenciais</a></li>
					<li><a class="d-md-none" href="<?php echo get_site_url() ?>/unidades">Unidades</a></li>
					<li><a href="<?php echo get_site_url() ?>/sustentabilidade">Sustentabilidade</a></li>
					<li><a href="<?php echo get_site_url() ?>/certificacoes">Certificações</a></li>
					<li><a href="<?php echo get_site_url() ?>/contato">Contato</a></li>
					<li><a href="https://mosca.com.br/wp-content/uploads/2024/11/RELATORIO-IGUALDADE-SALARIAL-MOSCA.pdf" target="_blank">Relatório de Igualdade Salarial</a></li>

				</ul>
				<img class="d-lg-none pt-5 h-50 " src="<?= get_stylesheet_directory_uri(); ?>/dist/img/top-transporte.png" alt="" class="top-do-transporte ">

			</div>

			<div class="divisor d-lg-none"></div>

			<div class="d-lg-flex justify-content-between">
				<div class="col-12 col-lg-6 pl-3 px-md-5 pb-3 pb-lg-0 colunas-footer  pl-lg-0 pl-2 pr-lg-0 ">

					<h2 class="titulo">Segmentos</h2>
					<ul>
						<?php
						$argsSegmentos = array(
							'post_type' => 'segmentos',
							'posts_per_page' => -1,
							'orderby' => 'title',
							'order' => 'ASC',
						);

						$segmentos = new WP_Query($argsSegmentos);
						if ($segmentos->have_posts()) : while ($segmentos->have_posts()) : $segmentos->the_post();
						?>
								<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
							<?php endwhile; ?>
						<?php endif; ?>
					</ul>

				</div>

				<div class="col-12 col-lg-4 pr-0 pl-3 pb-3 pb-lg-0  colunas-footer pr-lg-3 mr-3 p-md-0  mr-md-0  ">
					<h2 class="titulo">Mídia</h2>
					<ul>
						<li><a href="<?= get_stylesheet_directory_uri(); ?>/dist/files/release.pdf" download="release.pdf">Assessoria de Imprensa</a></li>
						<li class="my-lg-3 mb-3 tac"><a href="<?= get_stylesheet_directory_uri(); ?>/dist/files/tac.doc" download="tac.doc"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/TAC.svg" alt=""></a></li>
						<li><button type="button" data-toggle="modal" data-target="#exampleModal" class="btn-trabalhe">Trabalhe Conosco</button></li>


					</ul>
				</div>
			</div>
		</div>

		<div class="col-lg-2 col-12 pb-3 p-lg-0  pr-md-0">
			<div class="site-seguro mb-3">
				<div class="selo"></div>
				<!-- <img src="<?= get_stylesheet_directory_uri() ?>/dist/img/selo-seguro.png" alt="">
					<span class="texto-seguro">site 100%</br>Seguro</span> -->
			</div>

			<div class="text-center  endereco text-lg-start">
				<p class="cidade">Campinas, SP</p>

				<a href="https://www.google.com.br/maps/dir//Mosca+Log%C3%ADstica+-+Av.+Ant%C3%B4nio+Boscato,+171+-+TIC,+Campinas+-+SP,+13069-119/@-22.8445264,-47.1539319,17z/data=!4m9!4m8!1m0!1m5!1m1!1s0x94bf5d6503bc34b3:0xd137a60c00fb51d4!2m2!1d-47.152411!2d-22.8452045!3e0" target="_blank">
					<p class="info">Av. Antônio Buscato. 171 TIC<br>CEP: 13069-119</p>
				</a>

				<p class="mb-0"><a class="info" href="tel:+551937812222"><b><i class="fab fa-whatsapp text-white"></i> (19) 3781.2222</b></a><br>
					<a class="info" href="mailto:vendas@mosca.com.br">vendas@mosca.com.br</a>
				</p>

			</div>
		</div>
	</div>

	</div>
	</div>


	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog trabalhe-form" role="document">
			<div class="modal-content">
				<div class="modal-header justify-content-between">
					<img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/LOGO.svg" alt="">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<h5 class="modal-title" id="exampleModalLabel">Trabalhe Conosco</h5>
					</br>
					<p>A Mosca Logística tem uma oportunidade pra você!</p>
					</br>

					<div class="col-12 px-0">
						<?= do_shortcode('[contact-form-7 id="127" title="Trabalhe Conosco"]'); ?>
					</div>


				</div>

			</div>
		</div>
	</div>





</footer>

<!-- Principal JavaScript do Bootstrap -->
<?php wp_footer(); ?>
<script src="https://kit.fontawesome.com/58275e0b3b.js" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/min/tiny-slider.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
<script src="<?= get_stylesheet_directory_uri(); ?>/dist/js/app.js"></script>
</body>


<?php if (is_page('certificacoes')) : ?>

	<script>
		var sliderPremios = tns({
			container: ".carousel-certificacoes",
			mode: "carousel",
			slideBy: 1,
			autoplayButtonOutput: false,
			controls: false,
			nav: true,
			navPosition: 'bottom',
			gutter: 50,
			responsive: {
				300: {
					items: 2,
				},
				600: {
					items: 4,
				}
			}

		});
	</script>

<?php endif; ?>


</html>