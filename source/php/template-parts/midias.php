<section class="d-lg-block ">

    <div class="d-flex flex-column midias">
        <a target="_blank" class="square link" href="https://www.linkedin.com/company/mosca-log%C3%ADstica/?viewAsMember=true"><i class="fab fa-linkedin text-white" aria-hidden="true"></i></a>
        <a target="_blank" class="square insta" href="https://www.instagram.com/moscalogistica/"><i class="fab fa-instagram text-white" aria-hidden="true"></i></a>
        <a target="_blank" class="square face" href="https://www.facebook.com/moscalogistica"><i class="fab fa-facebook text-white" aria-hidden="true"></i> </a>
        <a target="_blank" class="square yt" href="https://www.youtube.com/channel/UCDMdr5YLEC0TeXCeMGF6k0g"><i class="fab fa-youtube text-white" aria-hidden="true"></i></a>
    </div>
</section>

<section class="d-block">
    <a class="whats-btn mt-5 mt-md-0" target="_blank" href="https://api.whatsapp.com/send?phone=5519993457390">
        <i class="fab fa-whatsapp fa-2x text-white"></i>
    </a>
</section>