<?php get_header(); ?>

<div class="banner  bg-sobre">
  <div class="filtro">
    <div class="p-150">
      <div id="detail"></div>
      <h1 class="text-white">Sobre</h1>

      <div class="circle">
        <i class="text-white fas fa-arrow-down"></i>
      </div>
    </div>
  </div>

  <a href="#" class="circle d-md-none c-detail">
    <i class="fas fa-arrow-up"></i>
  </a>

</div>

<section class="col-lg-10 px-0 m-auto">

  <div class=" col-lg-9  margin-center">
    <div class='py-3 pb-md-5 pt-md-4  '>
      <h2 class="color-blue col-md-9 font-weight-bold  p-0  pb-md-3 pb-3">O melhor caminho para a eficiência.</br class="d-none d-lg-block"> Da indústria até o varejo.</h2>
      <p class="color-text">
        <b>Especializada em logística fracionada, no abastecimento da indústria para o segmento supermercadista</b>, a Mosca Logística conta com vasta experiência no setor para garantir que o produto da sua empresa chegue até o varejo com segurança, agilidade e precisão, todo o tramite logístico é repleto de tecnologia e metodologia que permitem a eficiência no abastecimento dos PDV ’s dos seus clientes.</p>

      <p class="color-text">Possuímos cobertura de 100% do Estado de São Paulo para atender cada cliente de forma personalizada.</p>
    </div>
  </div>

  <div class="d-lg-flex col-lg-9 margin-center p-0 position-detail flex-row-reverse justify-content-end align-items-center">

    <div class=" image-sobre-1"></div>

    <div class="bg-blue text-white container col-12 m-md-0 col-lg-7 py-lg-4 px-md-5 pt-3 pb-4">

      <h2 class="fw-400 pb-2 pb-md-4 pb-lg-2">Logística especializada no varejo</h2>

      <div class="d-flex align-items-end justify-content-between d-lg-none">
        <div class="separador"></div>

      </div>

      <p>Somos especializados em logística fracionada para o abastecimento do segmento supermercadista.</p>
      <p>Veja alguns dos nossos benefícios:</p>

      <ul class="pl-0">
        <li><span class="pr-4">•</span> Módulo de agendamento</li>
        <li><span class="pr-4">•</span> Alta capacidade de processamento</li>
        <li><span class="pr-4">•</span> Transferência e Distribuição paletizada</li>
        <li><span class="pr-4">•</span> SAC dedicado a cada entrega</li>
        <li><span class="pr-4">•</span> 100% da Carga Unitizada</li>
        <li><span class="pr-4">•</span> Sistema de baixa via mobile “On Time”</li>
      </ul>

    </div>
  </div>
</section>


<div class="fake-carrosel text-md-center">
  <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/linha-do-tempo.png" alt="">


</div>

<div class="position-relative pb-4 d-lg-none">
  <div class="arrow arrow-first"></div>
  <div class="arrow arrow-second"></div>
</div>

<section class="missao-valores">

  <div class="container">

    <div class="wrapper">

      <div class="carousel-missao">


        <div class="item">

          <div class="blue col-12">

            <div class="green">
              <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/missao-icon.png" alt="">
            </div>

            <span class="title">Missão</span>

            <div class="line"></div>

            <p>Estar entre as melhores e mais desejadas empresas que oferecem soluções logísticas para os mercados regionais.</p>

          </div>

          <div class="line-bottom col-12"></div>

        </div>

        <div class="item">

          <div class="blue col-12">

            <div class="green">
              <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/visao-icon.png" alt="">
            </div>

            <span class="title">visão</span>

            <div class="line"></div>

            <p>Desenvolver, comercializar, e operacionalizar soluções logísticas, preservando a integridade dos produtos, para empresas que valorizam excelência nos serviços prestados, atendimento diferenciado e pontualidade nas entregas, atendendo a regiões e mercados estratégicos.</p>

            <a href="#" class="btn" data-toggle="modal" data-target="#exampleModal1">+ Leia Mais</a>

          </div>

          <div class="line-bottom col-12"></div>

        </div>

        <div class="item">

          <div class="blue col-12">

            <div class="green">
              <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/valores-icon.png" alt="">
            </div>

            <span class="title">valores</span>

            <div class="line"></div>

            <ul>
              <li>1) Comprometimento como caminho para a excelência.</li>
              <li>2) Valorização do desempenho das pessoas por mérito.</li>
              <li>3)Comunicação de forma clara e direta.</li>
              <li>4) Criatividade e tolerância a erros como meios de inovação.</li>
              <li>5) Segurança acima de tudo.</li>
              <li>6)Respeito à diversidade.</li>
              <li>7)Compromisso socioambiental.</li>
              <li>8)Transparência e integridade nas relações com os clientes, fornecedores e </li>
              <li>colaboradores.</li>
              <li>9)Compromisso com os resultados planejados.</li>
              <li>10)Prontidão para mudança com agilidade nas tomadas de decisões.</li>
              <li>11)Delegação sem a neura do microgerenciamento</li>
            </ul>

            <a href="#" class="btn" data-toggle="modal" data-target="#exampleModal2">+ Leia Mais</a>

          </div>

          <div class="line-bottom col-12"></div>

        </div>
      </div>
    </div>
  </div>

  <div class="position-relative pb-4 d-lg-none">
    <div class="arrow top-130 arrow-first"></div>
    <div class="arrow top-130 arrow-second"></div>
  </div>

</section>


<section class="consultoria pb-3 pt-5">

  <h1 class="px-5 pb-3 pt-md-4 col-lg-4 color-blue text-center fw-400">Quer ver tudo o que
    podemos fazer por você?</h1>

  <a class="btn-sobre mb-4" href="mailto:vendas@mosca.com.br">Solicite uma consultoria</a>

</section>


<?php get_template_part('midias'); ?>





<!-- Modal visão -->
<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-custom pb-5 " role="document">
    <div class="modal-content ">

      <div class="modal-missao">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>


        <div class="blue col-12">

          <div class="green">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/visao-icon.png" alt="">
          </div>

          <span class="title">visão</span>

          <div class="line"></div>

          <p>Desenvolver, comercializar, e operacionalizar soluções logísticas, preservando a integridade dos produtos, para empresas que valorizam excelência nos serviços prestados, atendimento diferenciado e pontualidade nas entregas, atendendo a regiões e mercados estratégicos.</p>


        </div>
      </div>

    </div>
  </div>
</div>


<!-- Modal valores -->
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-custom pb-5" role="document">
    <div class="modal-content ">

      <div class="modal-missao">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>


        <div class="blue col-12">

          <div class="green">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/valores-icon.png" alt="">
          </div>

          <span class="title">valores</span>

          <div class="line"></div>

          <ul>
            <li>1) Comprometimento como caminho para a excelência.</li>
            <li>2) Valorização do desempenho das pessoas por mérito.</li>
            <li>3)Comunicação de forma clara e direta.</li>
            <li>4) Criatividade e tolerância a erros como meios de inovação.</li>
            <li>5) Segurança acima de tudo.</li>
            <li>6)Respeito à diversidade.</li>
            <li>7)Compromisso socioambiental.</li>
            <li>8)Transparência e integridade nas relações com os clientes, fornecedores e </li>
            <li>colaboradores.</li>
            <li>9)Compromisso com os resultados planejados.</li>
            <li>10)Prontidão para mudança com agilidade nas tomadas de decisões.</li>
            <li>11)Delegação sem a neura do microgerenciamento</li>
          </ul>


        </div>
      </div>

    </div>
  </div>
</div>









<?php get_footer() ?>