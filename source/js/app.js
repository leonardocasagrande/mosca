var url = document.location.origin + "/mosca/";

if (document.URL !== url) {
  $("#btn-seg").remove();
}

if (window.innerWidth > 500 && document.URL !== url) {
  $("#btn-unt").remove();
}

$(document).ready(function () {
  var sliderBanner = tns({
    container: ".banner-carousel",
    mode: "carousel",
    items: "page",
    slideBy: 1,
    autoplayButtonOutput: false,
    autoplay: true,
    controls: true,
    controlsText:["<i class='fas fa-chevron-left'></i>", "<i class='fas fa-chevron-right'></i>"],
    nav: false,
    speed: 400,
    items: 1,
    // responsive: {
    //   200: {
    //     disable: true,
    //   },
    //   800: {
    //     disable: false,
    //   },
    // },
  });
});

$(document).ready(function () {
  var sliderBanner = tns({
    container: ".banner-carousel2",
    mode: "carousel",
    items: "page",
    slideBy: 1,
    autoplayButtonOutput: false,
    autoplay: true,
    controls: false,
    nav: false,
    speed: 400,
    items: 1,
    // responsive: {
    //   200: {
    //     disable: true,
    //   },
    //   800: {
    //     disable: false,
    //   },
    // },
  });
});

$(document).ready(function () {
  var sliderDepo = tns({
    container: ".depo-container",
    mode: "carousel",
    slideBy: 1,
    autoplayButtonOutput: false,
    autoplay: true,
    loop: true,
    controls: false,
    nav: false,
    items: 1,
    autoplayTimeout: 3000,
    responsive: {
      200: {
        items: 1,
      },
      500: {
        items: 2,
      },
      1000: {
        items: 2,
        fixedWidth: 420,
      },
      1100: {
        items: 3,
        fixedWidth: 300,
      },
      1600: {
        items: 3,
        fixedWidth: 400,
      },
    },
  });
});

// $(document).ready(function () {
//   var sliderBanner = tns({
//     container: ".btn-carousel-container",
//     mode: "carousel",
//     slideBy: 1,
//     autoplayButtonOutput: false,
//     autoplay: true,
//     autoplayTimeout: 7000,
//     controls: false,
//     nav: false,
//     loop: true,
//     items: 1,
//     responsive: {
//       300: {
//         items: 1,
//       },
//       800: {
//         items: 3,
//       },
//     },
//   });
// });

$(document).ready(function () {
  var sliderHome = tns({
    container: ".home-container",
    mode: "gallery",
    items: 1,
    slideBy: 1,
    autoplayButtonOutput: false,
    autoplay: false,
    controls: false,
    navContainer: ".dot-container",
  });

  $(".dot-container").click(function () {
    let info = sliderHome.getInfo().displayIndex;
    var count = info.toString();
    $(".counter").text(count);
  });
});

// $(document).ready(function () {
//   var slider = tns({
//     container: ".logos-carosel",
//     mode: "carousel",
//     items: 2,
//     slideBy: "2",
//     autoplayButtonOutput: false,
//     autoplay: true,
//     controls: false,
//     mouseDrag: true,
//     nav: false,
//     gutter: 40,
//     autoplayTimeout: 1500,
//     responsive: {
//       768: {
//         items: 4,
//       },
//       1024: {
//         items: 8,
//       },
//       1480: {
//         items: 9,
//       },
//     },
//   });
// });
$(document).ready(function () {
  var sliderValores = tns({
    container: ".carousel-missao",
    slideBy: 1,
    autoplayButtonOutput: false,
    autoplay: false,
    controls: false,
    loop: false,
    nav: false,
    responsive: {
      200: {
        items: 1.1,
      },
      760: {
        items: 2.1,
      },
      1000: {
        items: 3.2,
      },
    },
  });
});

$(document).ready(function () {
  var slider = tns({
    container: ".certifica-container",
    mode: "gallery",
    items: 1,
    slideBy: 1,
    autoplayButtonOutput: false,
    autoplay: false,
    controls: false,
    navContainer: ".dot-container",
  });
});

$(document).ready(function () {
  var slider = tns({
    container: ".carousel-depo-bebidas",
    items: 1,
    slideBy: 1,
    autoplayButtonOutput: false,
    autoplay: false,
    controls: false,
    navPosition: "bottom",
  });
});

$(document).ready(function () {
  var sliderOutrosSegmentos = tns({
    container: ".carousel-outros-segmentos",
    items: 1.4,
    slideBy: 1,
    autoplayButtonOutput: false,
    autoplay: false,
    controls: false,
    nav: false,
    loop: false,
    responsive: {
      1000: {
        disable: true,
      },
    },
  });
});

$(document).ready(function () {
  var sliderPdv = tns({
    container: ".carousel-pdv",
    items: 2.1,
    loop: false,
    slideBy: "page",
    autoplayButtonOutput: false,
    autoplay: false,
    controls: false,
    nav: false,
    responsive: {
      1000: {
        disable: true,
      },
    },
  });
});

$(".c-detail").click(function (e) {
  e.preventDefault();
  var body = $("html, body");
  body.stop().animate({ scrollTop: 0 }, 500, "swing");
});

$(".click-seg").click(function (e) {
  e.preventDefault();
  $("html, body").animate({
    scrollTop: $(".seg").offset().top - $(window).height() / 4,
  });

  console.log("chegou");
});

$("#unity").click(function (e) {
  e.preventDefault();
  if (window.innerWidth <= 500) {
    window.location.href = url + "/unidades";
  } else {
    $("html, body").animate({
      scrollTop: $(".mapa").offset().top - $(window).height() / 6,
    });
  }
});

// --------

function boxTop(idBox) {
  var boxOffset = $(idBox).offset();
  return boxOffset.top;
}

$(document).scroll(function () {
  var documentTop = $(this).scrollTop();

  if (documentTop > boxTop("#caminhao") - 600) {
    $(".caminhao").attr("style", "left:0px");
  }
});

$(document).ready(function () {
  var card = $(".seg-desk-home").children();
  card[0].classList.add("c1");
  card[1].classList.add("c2");
  card[2].classList.add("c3");
  card[3].classList.add("c4");
  card[4].classList.add("c5");
  card[5].classList.add("c6");

  var newLink = document.location.href;
  var bebidaLink = $(".c5 a")[0];
  bebidaLink.href = newLink + "bebidas";
});

$(document).ready(function () {
  var card = $(".seg-desk .the-line");
  card[0].classList.add("c1");
  card[1].classList.add("c2");
  card[2].classList.add("c3");
  card[3].classList.add("c4");
  card[4].classList.add("c5");
  card[5].classList.add("c6");

  var newLink = document.location.href;
  var bebidaLink = $(".card")[4].children[4];
  bebidaLink.href = newLink + "bebidas";
});

$(window).scroll(function () {
  if (window.pageYOffset > 15) {
    $(".navbar").addClass("nav-active");
  } else {
    $(".navbar").removeClass("nav-active");
  }
});

// $(".licenca-item").click(function (e) {
//   e.preventDefault();
// });

$(".btn-apn").click(function () {
  $(".wpcf7-response-output").addClass("d-none");
});

$(".select-form option:first-child").attr("disabled", "true");
$(".btn-file").addClass("d-lg-none");
